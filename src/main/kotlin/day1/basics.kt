package day1

fun calculateY(m : Int, x : Int, c : Int) : Int {
    // y = mx + c - formula for a straight line
    return m * x + c
}

fun intersection(mOne : Int, cOne : Int, mTwo : Int, cTwo : Int) : Unit {
    // This function takes the equations of two lines:
    // yOne = mOne * x + cOne and yTwo = mTwo * x + cTwo
    // and prints the X value they intersect at
    // or prints "Don't intersect" if they do not.
    // You only need to check x values between 0 and 100
//    Try out your function for the lines:
//
//    y = 3x + 5 and y = 4x + 2
//    y = 2x + 1 and y = 3x + 3
//    y = -2x + 3 and y = 3x - 2


    for (x in 0 .. 100) {
        var a: Int = calculateY(mOne, x, cOne)
        var b: Int = calculateY(mTwo, x, cTwo)
        if (a == b) {
            println("$x")
        } else
        {
            println("Don't intersect")
        }
    }

}