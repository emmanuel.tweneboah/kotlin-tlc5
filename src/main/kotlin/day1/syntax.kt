package day1

import java.lang.NumberFormatException

fun main() {
    var funds: Double = 100.0
    val pswd = "password"

    var input: String
    var password: String
    var cmd: List<String>

    fun balance(): Unit {
        println(funds)
    }

    fun deposit(depositAmount: Double): Unit{
        try{
            funds += depositAmount
            println("deposit $depositAmount")
        }catch (ex: IndexOutOfBoundsException){
            println("Invalid number")
        }catch (ex: NumberFormatException){
            println("Enter a number instead")
        }

    }

    fun withdraw(withdrawalAmount: Double): Unit {
        println("Input password :")
        password = readLine()!!
        if (password == pswd) {
            funds -= withdrawalAmount
            println("withdraw $withdrawalAmount")
        } else {
            println("Incorrect Password")
        }

    }
    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        try {
            when (cmd[0]) {
                "balance" -> balance()
                "deposit" -> deposit(cmd[1].toDouble())
                "withdraw" -> withdraw(cmd[1].toDouble())
                // Each command goes here...
                else -> println("Invalid command")
            }
        }catch (ex: IndexOutOfBoundsException){
            println("Invalid number")
        }catch (ex: NumberFormatException){
            println("Please enter a number")
        }
    }


}


