

fun main(args: Array<String>) {
    var funds : Double = 100.0
    val pswd = "password"

    var input : String
    var cmd : List<String>

    fun balance() : Unit {
        println(funds)
    }

    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        when (cmd[0]) {
           // "balance" -> println(funds)
            "balance" -> balance()

            // Each command goes here...

            else -> println("Invalid command")
        }

    }
}

fun calculateY(m : Int, x : Int, c : Int) : Int {
    // y = mx + c - formula for a straight line
    return m * x + c
}

